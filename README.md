# MyDrive  
demo: https://hema-mydrive.herokuapp.com/  
### Features   

- Upload only pdf and powerpoint files.  
- Search using file title in database (implemented using `django-haystack` & `Whoosh`)  
- Search using the content inside pdf and powerpoint files (implemented using `re.search` on files' text extracted using `pdfminer.six` for pdf and `python-pptx` for powerpoint)
- Uploaded files can be public which appear in search results for all users, or private which appear only for the owner
### How to use:  
##### Local:
- Install requirements.txt using pip.  
`pip install requirements.txt`  
  
- copy .env.example to .env  
- edit environments variable in .env according to your environment.  
- make migration and migrate your database  
`python manage.py makemigrations`  
`python manage.py migrate`  
- create a superuser.  
`python manage.py createsuperuser`  
- run server  
`python manage.py runserver`  
- Try the web app now  
go to the link printed by the `runserver` command and login using the credentials entered during `createsuperuser`

##### Heroku:  
- login to heroku  
`heroku login`  
- create new app (change the name app-mydrive for all the following commands)  
 `heroku create app-mydrive`  
- set heroku remote  
 `heroku git:remote -a hema-mydrive`  
 - login to heroku in browser and get postgresql url , put the url in place of the dots in the next command  
 `heroku config:set DATABASE_URL=postgres://........`
 - disable collect static  
 `heroku config:set DISABLE_COLLECTSTATIC=1`
 - to be able to use media files on heroku we set debug to true  
 `heroku config:set DEBUG=True`  
 - push the code & run the app  
 `git push heroku master`  
 `heroku ps:scale web=1`
- open your app url in the browser
