from django import forms

from drive.models import FileUpload


class FileUploadForm(forms.ModelForm):
    class Meta:
        model = FileUpload
        fields = ['title', 'file_path', 'is_private']
    # title = forms.CharField(max_length=200, required=True)
    # file_path = forms.CharField(max_length=200, required=True)
    # is_private = forms.BooleanField()
    # uploaded_at = forms.DateTimeField()
