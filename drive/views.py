import mimetypes
import os
import re

from django.db.models import Q
from pdfminer import high_level
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.urls import reverse
from haystack.inputs import AutoQuery
from haystack.query import SearchQuerySet
from pptx import Presentation

from MyDrive.settings import MEDIA_ROOT
from drive.forms import FileUploadForm
from drive.models import FileUpload

LOGIN_REDIRECT_URL = 'home'

def search_files(query):
    files = os.listdir(MEDIA_ROOT)
    matched_files = []
    for file in files:
        file_path = os.path.join(MEDIA_ROOT, file)
        kind = mimetypes.guess_type(file_path)[0]
        if 'pdf' in kind:
            extracted_text = high_level.extract_text(file_path, "")
            ResSearch = re.search(query, extracted_text)
            if ResSearch:
                matched_files.append(file)
        elif 'presentation' in kind:
            prs = Presentation(file_path)
            ResSearch = None
            for slide in prs.slides:
                for shape in slide.shapes:
                    if hasattr(shape, "text"):
                        ResSearch = re.search(query, shape.text)
                        if ResSearch:
                            matched_files.append(file)
                            break
                    if ResSearch:
                        break
                if ResSearch:
                    break
            pass
    return matched_files


@login_required
def index(request):
    if request.method == 'POST':
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            u = FileUpload.objects.create(
                title=data['title'],
                file_path=request.FILES['file_path'],
                is_private=data['is_private'],
                owner=request.user
            )
            u.save()
            return redirect(reverse('home'))
    else:
        form = FileUploadForm()
    if 'query' not in request.GET.keys():
        search = False
        uploaded_files = FileUpload.objects.filter(owner=request.user).all()
    else:
        search = True
        # uploaded_files = FileUpload.objects.filter(is_private=False).filter(title__contains=request.GET['query']).all()
        search_results = SearchQuerySet().filter(content=AutoQuery(request.GET['query'])).models(FileUpload)
        uploaded_files = [result.object for result in search_results if result.object and (result.object.is_private == False or result.object.owner==request.user)]
        files = search_files(request.GET['query'])
        uploaded_files_by_content = FileUpload.objects.filter(Q(owner=request.user) | Q(is_private=False)).filter(file_path__in=files).all()
        uploaded_files = list(set(uploaded_files + list(uploaded_files_by_content)))
    return render(request, "drive/index.html", {'form': form, 'uploaded_files': uploaded_files, 'search': search})


# @login_required
# def
