from django.contrib import admin

# Register your models here.
from drive.models import FileUpload

admin.site.register(FileUpload)
