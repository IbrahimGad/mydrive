import datetime

from django.contrib.auth import get_user_model
from django.db import models
from django.core.validators import FileExtensionValidator


class FileUpload(models.Model):
    class Meta:
        db_table = 'file_uploads'
    title = models.CharField(max_length=200)
    file_path = models.FileField(max_length=200, validators=[FileExtensionValidator(['pdf', 'ppt', 'pptx'])])
    is_private = models.BooleanField()
    uploaded_at = models.DateTimeField(auto_now_add=True)

    owner = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title
