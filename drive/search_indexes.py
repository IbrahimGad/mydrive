import datetime

from haystack import indexes

from drive.models import FileUpload


class FileUploadIndex(indexes.SearchIndex, indexes.Indexable):
    title = indexes.CharField(document=True, use_template=True)
    uploaded_at = indexes.DateTimeField(model_attr='uploaded_at')

    def get_model(self):
        return FileUpload

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(uploaded_at__lte=datetime.datetime.now())
