from django.urls import path

from drive.views import index

urlpatterns = [
    path('', index, name='home')
]
